
GPRBUILD  = gprbuild
GPRCLEAN = gprclean
GPRINSTALL = gprinstall
RM	= rm -rf

.PHONY: all build build_test run_test test clean

all: build build_test

test: build_test run_test

build:
	-$(GPRBUILD) src/numerical.gpr

build_test:
	-$(GPRBUILD) test/numerical.gpr

run_test: build_test
	-./bin/test

clean:
	-$(RM) ./bin/* ./src/.build/* ./test/.build/* ./test/test

