
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Generic_Real_Arrays; 
with Matrix, Matrix_Input, Matrix_IO; 
with Integer_Enter;
with Gauss_Jordan;
with Exceptions;

procedure Demo_Gauss_Jordan is

  subtype Dimension is Positive range 1 .. 6;
  Dim : Dimension;

begin
  New_Line;  
  Put_Line ("Résolution de système linéaire par la méthode de Gauss-Jordan.");
  New_Line;  

  declare 
    procedure Dimension_Enter is new Integer_Enter (Dimension);
  begin
    Dimension_Enter (Dim, Prompt => "Dimension du système", Help => True);
  end;

  declare

    package Float_Arrays is new Ada.Numerics.Generic_Real_Arrays (Float);
    use Float_Arrays;
    package Float_Matrix is new Matrix (Float, Float_Arrays);
    use Float_Matrix;
    package Float_Matrix_Input is new Matrix_Input (Float, Float_Arrays);
    use Float_Matrix_Input;
    package Float_Matrix_IO is new Matrix_IO (Float, Float_Arrays);
    use Float_Matrix_IO;

    A : Real_Matrix (1 .. Dim, 1 .. Dim);
    B : Real_vector (1 .. Dim);

  begin
    Get_Matrix (A);
    Get_Vector (B);
    Put_Line ("Résolution de A.X = B avec");
    Put_Line ("A=");
    Put (A);
    Put_Line ("B=");
    Put (B);

    declare
      procedure Float_Gauss_Jordan is new Gauss_Jordan (Float, Float_Arrays);
    begin
      Float_Gauss_Jordan (A, B);
      Put_Line ("Solution X=");
      Put (B);
    exception
      when Exceptions.Determinant_Nul => 
        Put_Line ("Le déterminant de A est nul.");
      when others => raise;
    end;
  end;

end Demo_Gauss_Jordan;
