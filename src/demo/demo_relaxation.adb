
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Generic_Real_Arrays; 
with Matrix, Matrix_Input, Matrix_IO;
with Integer_Enter, Float_Enter, Reponse;
with Relaxation;
with Exceptions;

procedure Demo_Relaxation is

  subtype Dimension is Positive range 2 .. 6;
  Dim : Dimension;

begin

  New_Line;  
  Put_Line ("Résolution de système linéaire par la méthode de relaxation.");
  New_Line;  

  declare 
    procedure Enter_Dimension is new Integer_Enter (Dimension);
  begin
    Enter_Dimension (Dim, Prompt => "Dimension du système", Help => True);
  end;

  declare

    package Float_Arrays is new Ada.Numerics.Generic_Real_Arrays (Float);
    package Float_Matrix is new Matrix (Float, Float_Arrays);
    use Float_Matrix;
    package Float_Matrix_Input is new Matrix_Input (Float, Float_Arrays);
    use Float_Matrix_Input;
    package Float_Matrix_IO is new Matrix_IO (Float, Float_Arrays);
    use Float_Matrix_IO;

    A : Float_Arrays.Real_Matrix (1 .. Dim, 1 .. Dim);
    B, X : Float_Arrays.Real_vector (1 .. Dim);

  begin

    Get_Matrix (A);
    Get_Vector (B);
    Put_Line ("Résolution de A.X = B avec");
    Put_Line ("A=");
    Put (A);
    Put_Line ("B=");
    Put (B);

    declare
      subtype Omega_Type is Float range 0.0001 .. 1.9999;
      procedure Enter_Omega is new Float_Enter (Omega_Type);
      Omega : Omega_Type;
      Iteration : Natural := 500;

      procedure Float_Relaxation is new Relaxation (Float, Float_Arrays);
    begin
      loop
        Enter_Omega (Omega, 
                     Prompt => "Paramètre de relaxation ", 
                     Help => True);
        Float_Relaxation (A, B, X, Omega, Iteration);
        Put_Line ("Solution X=");
        Put (X);
        exit when not Reponse ("Un autre essai");
      end loop;
    exception
      when Exceptions.Diagonale_Non_Dominante =>
        Put_Line ("La matrice A est inadéquate (Diagonale non dominante)");
      when Exceptions.Non_Convergence =>
        Put_Line ("Convergence trop lente ou divergence");
        declare
          package Natural_IO is new Integer_IO (Natural);
          package Omega_Type_IO is new Float_IO (Omega_Type);
          use Natural_IO, Omega_Type_IO;
        begin
          Put ("Omega = ");
          Put (Omega, 1, 3, 0);
          New_Line;
          Put ("Nombre d'itérations: ");
          Put (Iteration, Width => 1);
          New_Line;
        end;
    end;
  end;

end Demo_Relaxation;
