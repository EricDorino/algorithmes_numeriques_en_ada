
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Text_IO, Ada.Numerics.Generic_Real_Arrays; 
with Matrix, Matrix_Input, Matrix_IO;
with Integer_Enter;
with Jacobi; 
with Exceptions;

procedure Demo_Jacobi is

	subtype Dimension is Positive range 2 .. 6;
	Dim : Dimension;

begin

	New_Line;	
	Put_Line ("Résolution de système linéaire par la méthode de Jacobi.");
	New_Line;	

	declare 
		procedure Dimension_Enter is new Integer_Enter (Dimension);
	begin
		Dimension_Enter (Dim, Prompt => "Dimension du système", Help => True);
	end;

	declare
		package Float_Arrays is new Ada.Numerics.Generic_Real_Arrays (Float);
		package Float_Matrix is new Matrix (Float, Float_Arrays);
		use Float_Matrix;
		package Float_Matrix_Input is new Matrix_Input (Float, Float_Arrays);
		use Float_Matrix_Input;
		package Float_Matrix_IO is new Matrix_IO (Float, Float_Arrays);
		use Float_Matrix_IO;

		A : Float_Arrays.Real_Matrix (1 .. Dim, 1 .. Dim);
		B, X : Float_Arrays.Real_vector (1 .. Dim);

	begin

		Get_Matrix (A);
		Get_Vector (B);

		Put_Line ("Résolution de A.X = B avec");
		Put_Line ("A=");
		Put (A);
		Put_Line ("B=");
		Put (B);

		declare
      package Natural_IO is new Integer_IO (Natural);
      use Natural_IO;

			procedure Float_Jacobi is new Jacobi (Float, Float_Arrays);
      Iteration : Natural := 500;
		begin
			Float_Jacobi (A, B, X, Iteration);
			Put_Line ("Solution X=");
			Put (X);
      New_Line;
      Put (Iteration, Width => 1);
      Put_Line (" iterations.");
		exception
			when Exceptions.Diagonale_Non_Dominante =>
				Put_Line ("La matrice A est inadéquate (Diagonale non dominante)");
			when Exceptions.Non_Convergence =>
				Put_Line ("Convergence trop lente ou divergence");
		end;
	end;

end Demo_Jacobi;
