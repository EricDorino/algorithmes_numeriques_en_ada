
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Generic_Real_Arrays;
with Matrix, Matrix_Input, Matrix_IO;
with Integer_Enter; 
with Pivot_Gauss;
with Systeme_Triangulaire_Superieur;

procedure Demo_Pivots_Gauss is

  subtype Dimension is Positive range 1 .. 6;
  Dim : Dimension;

begin

  New_Line;  
  Put_Line ("Résolution de système linéaire par la méthode des pivots de Gauss.");
  New_Line;  

  declare 
    procedure Dimension_Enter is new Integer_Enter (Dimension);
  begin
    Dimension_Enter (Dim, Prompt => "Dimension du système", Help => True);
  end;

  declare
    package Float_Arrays is new Ada.Numerics.Generic_Real_Arrays (Float);
    package Float_Matrix is new Matrix (Float, Float_Arrays);
    package Float_Matrix_Input is new Matrix_Input (Float, Float_Arrays);
    package Float_Matrix_IO is new Matrix_IO (Float, Float_Arrays);
    use Float_Arrays, Float_Matrix, Float_Matrix_Input, Float_Matrix_IO;

    A : Real_Matrix (1 .. Dim, 1 .. Dim);
    B : Real_vector (1 .. Dim);

  begin
    Get_Matrix (A);
    Get_Vector (B);

    Put_Line ("Avant pivotage:");
    Put_Line ("A=");
    Put (A);
    Put_Line ("B=");
    Put (B);

    declare
      procedure Float_Pivot_Gauss is new Pivot_Gauss (Float, Float_Arrays);
      procedure Float_STS is new
        Systeme_Triangulaire_Superieur (Float, Float_Arrays);
    begin

      Float_Pivot_Gauss (A, B);

      Put_Line ("Après pivotage:");
      Put_Line ("A=");
      Put (A);
      Put_Line ("B=");
      Put (B);

      Float_STS (A, B, B);
      Put_Line ("Solution X=");
      Put (B);
    end;
  end;

end Demo_Pivots_Gauss;
