
with Ada.Text_IO, Ada.Numerics.Generic_Real_Arrays, 
     Matrix, Matrix_Input, Matrix_IO, Integer_Enter,
     Deflation, Exceptions;
use Ada.Text_IO;

procedure Demo_Deflation is

  subtype Dimension is Positive range 2 .. 6;
  Dim : Dimension;
  N_Valeurs_Propres : Positive;

begin

  New_Line;  
  Put_Line ("Valeurs propres d'une matrice par la méthode de déflation.");
  New_Line;  

  declare 

    procedure Dimension_Enter is new Integer_Enter (Dimension);


  begin

    Dimension_Enter (Dim, Prompt => "Dimension de la matrice", Help => True);

    declare
      subtype Nombre_Valeurs_Propres_Type is Positive range 1 .. Dim;
      procedure Nombre_Valeurs_Propres_Enter is 
        new Integer_Enter (Nombre_Valeurs_Propres_Type);
    begin
      Nombre_Valeurs_Propres_Enter (
          N_Valeurs_Propres,
          Prompt => "Nombre de valeurs propres à calculer",
          Help => True);
    end;
  end;

  declare

    package Float_Arrays is new Ada.Numerics.Generic_Real_Arrays (Float);
    package Float_Matrix is new Matrix (Float, Float_Arrays);
    use Float_Matrix;
    package Float_Matrix_Input is new Matrix_Input (Float, Float_Arrays);
    use Float_Matrix_Input;
    package Float_Matrix_IO is new Matrix_IO (Float, Float_Arrays);
    use Float_Matrix_IO;

    M : Float_Arrays.Real_Matrix (1 .. Dim, 1 .. Dim);
    Valeurs_Propres : Float_Arrays.Real_Vector (1 .. N_Valeurs_Propres);
    Vecteurs_Propres : Float_Arrays.Real_Matrix (1 .. Dim, 1 .. N_Valeurs_Propres);
    S : Float;

  begin

    Get_Matrix (M);
    S := Trace (M);
    Put_Line ("Entrée =");
    Put (M);
    New_Line;
    declare
      procedure Float_Deflation is new Deflation (Float, Float_Arrays);
    begin
      Float_Deflation (M, Valeurs_Propres, Vecteurs_Propres);
      Put_Line ("Valeurs propres = ");
      Put (Valeurs_Propres);
      New_Line;
      Put_Line ("Vecteurs propres = ");
      Put (Vecteurs_Propres);
      New_Line;
    exception
      when Exceptions.NON_CONVERGENCE =>
        Put_Line("Attention: La méthode ne converge pas.");
      when others => raise;
    end;
  end;

end Demo_Deflation;
