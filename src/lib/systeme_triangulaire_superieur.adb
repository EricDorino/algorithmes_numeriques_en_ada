With Exceptions;

procedure Systeme_Triangulaire_Superieur (A : Real_Matrix; 
                                        B : Real_Vector;
                                        X : out Real_Vector) is
  S : Real;  
begin
  if A'Length (1) /= A'Length (2) or else
     A'Length (1) /= B'length then
    raise Exceptions.Dimensions_Incorrectes;
  end if;

  begin
    X (X'Last) := B (X'Last) / A (X'Last, X'Last);
  exception
    when Numeric_Error =>
      raise Exceptions.Determinant_Nul;
  end;

  for I in reverse X'First .. X'Last -1 loop
    S := 0.0;
    for J in I +1 .. X'Last loop
      S := S + A (I, J) * B (J);
    end loop;
    begin
      X (I) := (B (I) - S) / A (I, I);
    exception
      when Numeric_Error =>
        raise Exceptions.Determinant_Nul;
    end;
  end loop;
end Systeme_Triangulaire_Superieur;
