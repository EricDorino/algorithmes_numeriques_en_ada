
With Text_IO; use Text_IO;
with Matrix, Exceptions;

procedure Relaxation (A : Real_Matrix;
                      B : Real_Vector;
                      X : out Real_Vector;
                      Omega : Real;
                      Iteration : in out Natural;
                      Epsilon : in Real := 1.0E-12) is

  Max_Iteration : constant Natural := Iteration;
  Continue : Boolean := true;
  Residu, Norme_Residu : Real;

  package Float_Matrix is new Matrix (Real, Real_Arrays);
  use Float_Matrix;

begin
  Iteration := 0;

  if not Est_Diagonale_Dominante (A) then
    raise Exceptions.Diagonale_Non_Dominante;
  end if;

  for I in B'Range loop
    X (I) := B (I) / A (I, I);
  end loop;

  while Iteration < Max_Iteration and Continue loop
    Iteration := Iteration + 1;
    Norme_Residu := 0.0;
    for I in A'Range (1) loop
      Residu := -B (I);
      for J in A'Range (2) loop
        Residu := Residu + A (I, J) * X (J);
      end loop;
      Residu := Residu / A (I, I);
      X (I) := X (I) - Omega * Residu;
      if abs Residu > Norme_Residu then
        Norme_residu := abs Residu;
      end if;
    end loop;
    Continue := Norme_Residu > Epsilon;
  end loop;

  if Continue then
    raise Exceptions.Non_Convergence;
  end if;

end Relaxation;
