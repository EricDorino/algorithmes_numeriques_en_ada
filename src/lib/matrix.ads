
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Generic_Real_Arrays;

generic

  type Real is digits <>;

  with package Real_Arrays is new Ada.Numerics.Generic_Real_Arrays(Real);
  use Real_Arrays;

package Matrix is

  procedure Matrice_Aleatoire (M : out Real_Matrix);
  procedure Matrice_Symetrique_Aleatoire (M : out Real_Matrix);
  procedure Matrice_De_Hilbert (M : out Real_Matrix);
  procedure Matrice_Test (M : out Real_Matrix);

  function Est_Diagonale_Dominante (M : Real_Matrix) return Boolean;
  function Trace (M : Real_Matrix) return Real;

  procedure Vecteur_Aleatoire (V : out Real_Vector);

  function Norme_Euclidienne (V : Real_Vector) return Real;
  function Norme_Du_Sup (V : Real_Vector) return Real;

  Matrix_Error : Exception;

end Matrix;
