
with Ada.Numerics.Generic_Real_Arrays;

generic

  type Real is digits <>;

  with package Real_Arrays is new Ada.Numerics.Generic_Real_Arrays (Real);
  use Real_Arrays;

-- Calcul des p premières valeurs propres avec les vecteurs propres associés.
--
-- Pour j = 1, ..., p, Valeurs_Propres(j) est la valeur propre numéro j, et
-- Vecteurs_Propres(i, j) sont les composantes d'un vecteur propre associé.

procedure Deflation (M : Real_Matrix; 
                     Valeurs_Propres : out Real_Vector; 
                     Vecteurs_Propres : out Real_Matrix;
                     Epsilon : in Real := 1.0E-12;
                     Max_Iteration : in Natural := 500);
