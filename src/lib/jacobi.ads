
with Ada.Numerics.Generic_Real_Arrays;

generic
  type Real is digits <>;
  with package Real_Arrays is new Ada.Numerics.Generic_Real_Arrays (Real);
  use Real_Arrays;

procedure Jacobi (A : Real_Matrix;
                  B : Real_Vector;
                  X : out Real_Vector;
                  Iteration : in out Natural;
                  Epsilon : Real := 1.0E-12);
