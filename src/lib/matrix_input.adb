
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded; 
with Matrix;
with Matrix_IO;
with Menu;

package body Matrix_Input is

  procedure Get_Matrix (Data : out Real_Matrix) is

    subtype Result is Positive range 1..8;
    package Matrix_Menu is new Menu (Result);
    use Matrix_Menu;

    C : Choice := (
      To_Unbounded_String ("matrice lue au clavier"),
      To_Unbounded_String ("matrice aléatoire"),
      To_Unbounded_String ("matrice lue dans un fichier"),
      To_Unbounded_String ("matrice de Hilbert"),
      To_Unbounded_String ("matrice test symétrique, telle que M (i, j) = n - j + 1"),
      To_Unbounded_String ("matrice symétrique lue au clavier"),
      To_Unbounded_String ("matrice symétrique aléatoire"),
      To_Unbounded_String ("matrice symétrique lue dans un fichier")
    );
    N : Result;

    package M is new Matrix (Real, Real_Arrays);
    package MIO is new Matrix_IO (Real, Real_Arrays);
    use M, MIO;

  begin

    Put_Line ("Matrice du système");
    Enter (C, N);
    case N is
      when 1 =>
        Get (Data);
      when 2 =>
        Matrice_Aleatoire (Data);
      when 3 =>
        null;
      when 4 =>
        Matrice_De_Hilbert (Data);
      when 5 =>
        Matrice_Test (Data);
      when 6 =>
        Get (Data); -- Vérifier la symétrie
      when 7 =>
        Matrice_Symetrique_Aleatoire (Data);
      when 8 =>
        null;
    end case;  

  end Get_Matrix;

  procedure Get_Vector (Data : out Real_Vector) is

    subtype Result is Positive range 1..3;
    package Vector_Menu is new Menu (Result);
    use Vector_Menu;

    C : Choice := (
      To_Unbounded_String ("vecteur lu au clavier"),
      To_Unbounded_String ("vecteur aléatoire"),
      To_Unbounded_String ("vecteur lu dans un fichier")
    );

    N : Result;

    package M is new Matrix (Real, Real_Arrays);
    package MIO is new Matrix_IO (Real, Real_Arrays);
    use M, MIO;

  begin

    Put_Line ("Vecteur du système");
    Enter (C, N);
    case N is
      when 1 =>
        Get (Data);
      when 2 =>
        Vecteur_Aleatoire (Data);
      when 3 =>
        null;
    end case;

  end Get_Vector;

end Matrix_Input;
