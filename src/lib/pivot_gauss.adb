With Swap;
with Exceptions;

procedure Pivot_Gauss (A : in out Real_Matrix; 
                       B : in out Real_Vector;
                       Epsilon : in Real := 1.0E-12) is

  procedure Pivot_Max (K : Natural;
                       A : in out Real_Matrix;
                       B : in out Real_Vector;
                       Epsilon : in Real) is
    procedure Swap_Real is new Swap (Real);
    L : Natural := K;
  begin
    for I in K + 1 .. A'Last (1) loop
      if abs (A (I, K)) > abs (A (L, K)) then
        L := I;
      end if;
    end loop;

    if abs (A (L, K)) < Epsilon then
      raise Exceptions.Determinant_Nul;
    end if;

    if L /= K then
      for J in K .. A'Last (2) loop
        Swap_Real (A (K, J), A (L, J));
      end loop;
      Swap_Real (B (K), B(L));
    end if;
  end Pivot_Max;

  procedure Eliminer (K : Natural;
                      A : in out Real_Matrix;
                      B : in out Real_Vector) is
    M : Real;
  begin
    for I in K + 1 .. A'Last (2) loop
      M := A (I, K) / A (K, K);
      for J in K + 1 .. A'Last (2) loop
        A (I, J) := A (I, J) - M * A (K, J);
      end loop;
      A (I, K) := 0.0;
      B (I) := B (I) - M * B (K);
    end loop;
  end Eliminer;
  
begin
  for K in B'First .. B'Last - 1 loop
    Pivot_Max (K, A, B, Epsilon);
    Eliminer (K, A, B);
  end loop;
end Pivot_Gauss;
