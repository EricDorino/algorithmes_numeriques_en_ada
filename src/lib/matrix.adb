
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded; 
with Ada.Numerics.Generic_Elementary_Functions; 
with Ada.Numerics.Float_Random; use Ada.Numerics.Float_Random; 
with Matrix_IO;
with Menu;

package body Matrix is

  procedure Matrice_Aleatoire (M : out Real_Matrix) is
    Gen : Generator;
  begin

    Reset (Gen);
    for I in M'Range (1) loop
      for J in M'Range (2) loop
        M (I, J) := -20.0 + Real (Random (Gen)) * 40.0;
      end loop;
    end loop;

  end Matrice_Aleatoire;

  procedure Matrice_De_Hilbert (M : out Real_Matrix) is
  begin

    if M'Length (1) /= M'Length (2) then
      raise Matrix_Error;
    end if;

    for I in M'Range (1) loop
      for J in M'Range (2) loop
        M (I, J) := 1.0 / Real (I + J - 1);
      end loop;
    end loop;

  end Matrice_De_Hilbert;

  procedure Matrice_Test (M : out Real_Matrix) is
  begin

    if M'Length (1) /= M'Length (2) then
      raise Matrix_Error;
    end if;

    for I in M'Range (1) loop
      for J in I .. M'Last (2) loop
        M (I, J) := Real (M'Last (2) - J + 1);
      end loop;
    end loop;

    for I in 2 .. M'Last (1) loop
      for J in M'First (1) .. I - 1 loop
        M (I, J) := M (J, I);
      end loop;
    end loop;

  end Matrice_Test;

  procedure Matrice_Symetrique_Aleatoire (M : out Real_Matrix) is
    Gen : Generator;
  begin

    if M'Length (1) /= M'Length (2) then
      raise Matrix_Error;
    end if;

    Reset (Gen);

    for I in M'Range (1) loop
      for J in M'Range (2) loop
        M (I, J) := -20.0 + Real (Random (Gen)) * 40.0;
      end loop;
      for J in M'First (2) .. I - 1 loop
        M (I, J) := M (J, I);
      end loop; 
    end loop;

  end Matrice_Symetrique_Aleatoire;

  procedure Vecteur_Aleatoire (V : out Real_Vector) is
    Gen : Generator;
  begin

    Reset (Gen);

    for I in V'Range loop
        V (I) := -20.0 + Real (Random (Gen)) * 40.0;
    end loop;

  end Vecteur_Aleatoire;

  function Est_Diagonale_Dominante (M : Real_Matrix) return Boolean is
    S : Real;
  begin
    if M'Length (1) /= M'Length (2) then
      raise Matrix_Error;
    end if;

    for I in M'Range (1) loop
      S := 0.0;
      for J in M'Range (2) loop 
        if J /= I then
          S := S + abs (M (I, J));
        end if;
      end loop;
      if abs (M (I, I)) < S then
        return False;
      end if;  
    end loop;

    return True;
  end Est_Diagonale_Dominante;

  function Norme_Du_Sup (V : Real_Vector) return Real is
    Aux : Real := abs (V (V'First));
  begin
    for I in V'First + 1 .. V'Last loop
      if abs (V (I)) > Aux then
        Aux := abs (V (I));
      end if;
    end loop;
    return Aux;
  end Norme_Du_Sup;

  function Norme_Euclidienne (V : Real_Vector) return Real is
    package Functions is new
      Ada.Numerics.Generic_Elementary_Functions (Real);
    use Functions;
    Aux : Real := 0.0;
  begin
    for I in V'Range loop
      Aux := Aux + V (I) * V (I);
    end loop;
    return Sqrt (Aux);
  end Norme_Euclidienne;

  function Trace (M : Real_Matrix) return Real is
    Aux : Real := 0.0;
  begin
    if M'Length (1) /= M'Length (2) then
      raise Matrix_Error;
    end if;
    for I in M'Range (1) loop
      Aux := Aux + M (I, I);
    end loop;
    return Aux;
  end Trace;  

end Matrix;
