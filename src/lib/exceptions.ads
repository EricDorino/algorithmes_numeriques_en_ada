
package Exceptions is 
	
	Determinant_Nul : exception;
	Dimensions_Incorrectes : exception;
	Diagonale_Non_Dominante : exception;
	Non_Convergence : exception;

end Exceptions;
