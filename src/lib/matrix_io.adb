
with Ada.Text_IO; use Ada.Text_IO;
with Is_TTY;

package body Matrix_IO is

  procedure Get (File : File_Type; Item : out Real_Vector) is
    package Real_IO is new Float_IO (Real);
    use Real_IO;
  begin
    for I in Item'Range loop
      Get (File, Item (I));
      Skip_Line (File);
    end loop;
  end Get;

  procedure Get (Item : out Real_Vector) is
    package Int_IO is new Integer_IO (Integer);
    package Real_IO is new Float_IO (Real);
    use Int_IO, Real_IO;
  begin
    if Is_TTY then
      for I in Item'Range loop
        loop
          begin
            Put (I, Width => 1);
            Put ("=");
            Get (Item (I));
            exit;
          exception
            when Data_Error | Constraint_Error => Skip_Line;
          end;
        end loop;
      end loop;
    else
      Get (Standard_Input, Item);
    end if;
  end Get;

  procedure Put (File : File_Type;
                 Item : Real_Vector;
                 Fore: Field := 6;
                 Aft: Field := 4;
                 Exp: Field := 0) is
    package Int_IO is new Integer_IO (Integer);
    package Real_IO is new Float_IO (Real);
    use Int_IO, Real_IO;
  begin
    for I in Item'First .. Item'Last loop
      Put (File, Item (I), Fore, Aft, Exp);
      New_Line (File);
    end loop;
  end Put;

  procedure Put (Item : Real_Vector;
                 Fore: Field := 6;
                 Aft: Field := 4;
                 Exp: Field := 0) is
    package Real_IO is new Float_IO (Real);
    use Real_IO;
  begin

    if Is_TTY then
      Put ("(");
      for I in Item'First .. Item'Last - 1 loop
        Put (Item (I), Fore, Aft, Exp);
        Put (", ");
      end loop;
      Put (Item (Item'Last), Fore, Aft, Exp);
      Put_Line (")");
    else
      Put (Standard_Output, Item, Fore, Aft, Exp);
    end if;
  end Put;

  procedure Get (File : File_Type; Item : out Real_Matrix) is
     package Real_IO is new Float_IO (Real);
    use Real_IO;
  begin
    for I in Item'Range (1) loop
      for J in Item'Range (2) loop
        Get(File, Item (I, J));
        Skip_Line (File);
      end loop;
    end loop;
  end Get;

  procedure Get (Item : out Real_Matrix) is
    package Int_IO is new Integer_IO (Integer);
    package Real_IO is new Float_IO (Real);
    use Int_IO, Real_IO;
  begin
    if Is_TTY then  
      for I in Item'Range (1) loop
        for J in Item'Range (2) loop
          loop
            begin
              Put ("(");
              Put (I, Width => 1);
              Put (", ");
              Put (J, Width => 1);
              Put (")=");
              Get (Item (I, J));
              exit;
            exception
              when Data_Error | Constraint_Error => Skip_Line;
            end;
          end loop;
        end loop;
      end loop;
    else
      Get (Standard_Input, Item);
    end if;
  end Get;

  procedure Put (File: File_Type;
                 Item : Real_Matrix;
                 Fore: Field := 6;
                 Aft: Field := 4;
                 Exp: Field := 0) is
    package Int_IO is new Integer_IO (Integer);
    package Real_IO is new Float_IO (Real);
    use Int_IO, Real_IO;
  begin
    for I in Item'Range (1) loop
      Put (File, "(");
      for J in Item'Range (2) loop
        Put (File, Item (I, J), Fore, Aft, Exp);
        New_Line (File);
      end loop;
    end loop;
  end Put;

  procedure Put (Item : Real_Matrix;
                 Fore: Field := 6;
                 Aft: Field := 4;
                  Exp: Field := 0) is
    package Real_IO is new Float_IO (Real);
    use Real_IO;
  begin
    if Is_TTY then
      for I in Item'Range (1) loop
        Put ("(");
        for J in Item'First (2) .. Item'Last (2) - 1 loop
          Put (Item (I, J), Fore, Aft, Exp);
          Put (", ");
        end loop;
          Put (Item (I, Item'Last (2)), Fore, Aft, Exp);
        Put_Line (")");
      end loop;
    else 
      Put(Standard_Output, Item, Fore, Aft, Exp);
    end if;
  end Put;

end Matrix_IO;
