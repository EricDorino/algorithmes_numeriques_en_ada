
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Generic_Real_Arrays;

generic

  type Real is digits <>;

  with package Real_Arrays is new Ada.Numerics.Generic_Real_Arrays(Real);
  use Real_Arrays;

package Matrix_Input is

  procedure Get_Matrix (Data : out Real_Matrix);
  procedure Get_Vector (Data : out Real_Vector);

end Matrix_Input;
