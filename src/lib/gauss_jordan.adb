
with Swap;
with Exceptions;

procedure Gauss_Jordan (A : in out Real_Matrix;
                        B : in out Real_Vector;
                        Epsilon : in Real := 1.0E-12) is
  
  procedure Pivot_Max (K : Natural;
                       A : in out Real_Matrix;
                       B : in out Real_Vector;
                       Epsilon : in Real) is
    L : Natural := K;
    procedure Swap_Real is new Swap (Real);
  begin
    for I in K + 1 .. A'Last (1) loop
      if abs (A (I, K)) > abs (A (L, K)) then
        L := I;
      end if;
    end loop;

    if abs (A (L, K)) < Epsilon then
      raise Exceptions.DETERMINANT_NUL;
    end if;

    if L /= K then
      for J in K .. A'Last (2) loop
        Swap_Real (A (K, J), A (L, J));
      end loop;
      Swap_Real (B (K), B (L));
    end if;

  end Pivot_Max;

  procedure Eliminer (K : Natural; 
                      A : in out Real_Matrix; 
                      B : in out Real_Vector) is
  begin
    for J in K + 1 .. A'Last (2) loop
      A (K, J) := A (K, J) / A (K, K);
    end loop;

    B (K) := B (K) / A (K, K);
    A (K, K) := 1.0;

    for I in A'First (1) .. K - 1 loop
      for J in K + 1 .. A'Last (2) loop
        A (I, J) := A (I, J) - A (I, K) * A (K, J);
      end loop;
      B (I) := B (I) - A (I, K) * B (K);
      A (I, K) := 0.0;
    end loop;

    for I in K + 1 .. A'Last (1) loop
      for J in K + 1 .. A'Last (2) loop
        A (I, J) := A (I, J) - A (I, K) * A (K, J);
      end loop;
      B (I) := B (I) - A (I, K) * B (K);
      A (I, K) := 0.0;
    end loop;
  end Eliminer;

begin
  for K in A'Range (1) loop
    Pivot_Max (K, A, B, Epsilon);
    Eliminer (K, A, B);
  end loop;
end Gauss_Jordan;
