
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Generic_Real_Arrays;

generic

  type Real is digits <>;

  with package Real_Arrays is new Ada.Numerics.Generic_Real_Arrays(Real);
  use Real_Arrays;

package Matrix_IO is


  procedure Get (File : File_Type; Item : out Real_Vector);
  procedure Get (Item : out Real_Vector);

  procedure Put 
    (File : File_Type;
     Item : Real_Vector;
     Fore: Field := 6;
     Aft: Field := 4;
     Exp: Field := 0);
  procedure Put 
    (Item : Real_Vector;
     Fore: Field := 6;
     Aft: Field := 4;
     Exp: Field := 0);

  procedure Get (File : File_Type; Item : out Real_Matrix);
  procedure Get (Item : out Real_Matrix);

  procedure Put 
    (File : File_Type;
     Item : Real_Matrix;
     Fore: Field := 6;
     Aft: Field := 4;
     Exp: Field := 0);
  procedure Put 
    (Item : Real_Matrix;
     Fore: Field := 6;
     Aft: Field := 4;
     Exp: Field := 0);

end Matrix_IO;
