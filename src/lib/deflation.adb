
With Matrix;
With Exceptions;

procedure Deflation (M : Real_Matrix; 
                     Valeurs_Propres : out Real_Vector; 
                     Vecteurs_Propres : out Real_Matrix;
                     Epsilon : in Real := 1.0E-12;
                     Max_Iteration : in Natural := 500) is

  package Float_Matrix is new Matrix (Real, Real_Arrays);
  use Float_Matrix;

  VK, V, V_Ancien : Real_Vector (M'Range (1));
  MM : Real_Matrix (M'Range (1), M'Range (2)) := M;
  Aux, Aux1 : Real;
  Iteration, Numero : Natural;

begin

  for K in Vecteurs_Propres'Range (2) loop
    Vecteur_Aleatoire (VK);  
    Iteration := 0;
    loop  
      Iteration := Iteration + 1;
      V := MM * VK;
      Numero := 1;
      for I in 2 .. M'Last (1) loop
        if abs (VK (I)) > abs (VK (Numero)) then
          Numero := I;
        end if;
      end loop;
      Valeurs_Propres (K) := V (Numero) / VK (Numero);
      Aux := Norme_Euclidienne (V);
      Aux1 := 1.0 / Aux;
      V := Aux1 * V;
      V_Ancien := VK;
      VK := V;
      exit when Norme_Du_Sup (V_Ancien - VK) < Epsilon or 
                Iteration >= Max_iteration;
    end loop;
    for I in M'Range (1) loop
      Vecteurs_Propres (I, K) := V (I);
    end loop;
    if Iteration >= Max_Iteration then
      raise Exceptions.Non_Convergence;
    end if;
    for I in MM'Range (1) loop
      for J in MM'Range (2) loop
        MM (I, J) := MM(I, J) - Valeurs_Propres (K) * VK (I) * VK (J);
      end loop;
    end loop;
  end loop;

end Deflation;
