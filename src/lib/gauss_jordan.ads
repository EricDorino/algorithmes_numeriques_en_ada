
with Ada.Numerics.Generic_Real_Arrays;

generic
  type Real is digits <>;
  with package Real_Arrays is 
    new Ada.Numerics.Generic_Real_Arrays (Real);
  use Real_Arrays;

procedure Gauss_Jordan (A : in out Real_Matrix;
                        B : in out Real_Vector;
                        Epsilon : in Real := 1.0E-12);
