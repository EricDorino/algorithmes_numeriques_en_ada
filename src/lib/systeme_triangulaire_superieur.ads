
with Ada.Numerics.Generic_Real_Arrays;

generic
  type Real is digits <>;
  with package Real_Arrays is new Ada.Numerics.Generic_Real_Arrays (Real);
  use Real_Arrays;

procedure Systeme_Triangulaire_Superieur (A : Real_Matrix; 
                                          B : Real_Vector;
                                          X : out Real_Vector);
