With Ada.Text_IO; use ada.Text_IO;
with Matrix;
with Exceptions;

procedure Jacobi (A : Real_Matrix;
                  B : Real_Vector; 
                  X : out Real_Vector;
                  Iteration : in out Natural;
                  Epsilon : in Real := 1.0E-12) is

  package Float_Matrix is new Matrix (Real, Real_Arrays);
  use Float_Matrix;

  Max_Iteration : Natural := Iteration;
  Continue : Boolean := True;
  S : Real;
  V : Real_Vector (B'Range);

begin

  Iteration := 0;

  if not Est_Diagonale_Dominante (A) then
    raise Exceptions.Diagonale_Non_Dominante;
  end if;

  for I in B'Range loop
    X (I) := B (I) / A (I, I);
  end loop;

  while Iteration < Max_Iteration and Continue loop
    Iteration := Iteration + 1;
    for I in A'Range (1) loop
      S := B (I);
      for J in A'Range (2) loop
        if J /= I then
          S := S - A (I, J) * X (J);
        end if;
      end loop;
      V (I) := S / A (I, I);
    end loop;
    Continue := Norme_Du_Sup (V - X) > Epsilon;
    X := V;
  end loop;

  if Continue then
    raise Exceptions.Non_Convergence;
  end if;

end Jacobi;
