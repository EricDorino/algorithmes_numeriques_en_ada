
with Swap;
with Exceptions;

function Inverse_Gauss_Jordan (M : Real_Matrix;
                               Epsilon : in Real := 1.0E-12) 
         return Real_Matrix is
  
  procedure Pivot_Max (K : Natural;
                       A, Inv : in out Real_Matrix;
                       Epsilon : Real) is
    L : Natural := K;
    procedure Swap_Real is new Swap (Real);
  begin
    for I in K + 1 .. A'Last (1) loop
      if abs (A (I, K)) > abs (A (L, K)) then
        L := I;
      end if;
    end loop;

    if abs (A (L, K)) < Epsilon then
      raise Exceptions.DETERMINANT_NUL;
    end if;

    if L /= K then
      for J in K .. A'Last (2) loop
        Swap_Real (A (K, J), A (L, J));
      end loop;
      for J in A'Range (2) loop
        Swap_Real (Inv (K, J), Inv (L, J));
      end loop;
    end if;
  end Pivot_Max;

  procedure Eliminer (K : Natural; 
                      A, Inv : in out Real_Matrix) is
  begin
    for J in K + 1 .. A'Last (2) loop
      A (K, J) := A (K, J) / A (K, K);
    end loop;

    for J in A'Range (1) loop
      Inv (K, J) := Inv (K, J) / A (K, K);
    end loop;    

    A (K, K) := 1.0;

    for I in A'First (1) .. K - 1 loop
      for J in K + 1 .. A'Last (2) loop
        A (I, J) := A (I, J) - A (I, K) * A (K, J);
      end loop;
      for J in A'Range (2) loop
        Inv (I, J) := Inv (I, J) - A (I, K) * Inv (K, J);
      end loop;
      A (I, K) := 0.0;
    end loop;

    for I in K + 1 .. A'Last (1) loop
      for J in K + 1 .. A'Last (2) loop
        A (I, J) := A (I, J) - A (I, K) * A (K, J);
      end loop;
      for J in A'Range (2) loop
        Inv (I, J) := Inv (I, J) - A (I, K) * Inv (K, J);
      end loop;
      A (I, K) := 0.0;
    end loop;
  end Eliminer;

  M1, Inv : Real_Matrix (M'Range (1), M'Range (2));

begin
  M1 := M;
  Inv := (others => (others => 0.0));

  for I in M'Range (1) loop
    Inv (I, I) := 1.0;
  end loop;

  for K in M'Range (1) loop
    Pivot_Max (K, M1, Inv, Epsilon);
    Eliminer (K, M1, Inv);
  end loop;

  return Inv;

end Inverse_Gauss_Jordan;
