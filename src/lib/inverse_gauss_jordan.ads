
with Ada.Numerics.Generic_Real_Arrays;

generic
  type Real is digits <>;
  with package Real_Arrays is 
      new Ada.Numerics.Generic_Real_Arrays (Real);
  use Real_Arrays;
function Inverse_Gauss_Jordan (M : Real_Matrix;
                               Epsilon : in Real := 1.0E-12) 
         return Real_Matrix;
