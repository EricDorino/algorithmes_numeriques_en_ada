
procedure Swap (X, Y: in out Data) is
  Tmp: Data := X;
begin
  X := Y;
  Y := Tmp;
end Swap;
