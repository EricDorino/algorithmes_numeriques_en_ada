
with Text_IO; use Text_IO;
with Integer_Enter;

package body Menu is

  procedure Enter (C : Choice; N: out Result) is
    procedure Result_Enter is new Integer_Enter (Result);
    package Result_IO is new Integer_IO (Result);
    use Result_IO;
  begin
    for I in C'Range loop
      Put_Line (Result'Image (I) & ": " & To_String (C (I)));
    end loop;
    Result_Enter (N);
  end Enter;

end Menu;
