
with Text_IO; use Text_IO;

function Reponse (Prompt : String := "") return Boolean is
  R : Character;
begin
  loop
    begin
      Put (Prompt);
      Put ("? ");
      Get (R);
      Skip_Line;
      if R = 'o' or R = 'O' then
        return True;
      elsif R = 'n' or R = 'N' then
        return False;
      end if;
    exception
      when Data_Error | Constraint_Error =>
        Skip_Line;
    end;
  end loop;
end Reponse;
