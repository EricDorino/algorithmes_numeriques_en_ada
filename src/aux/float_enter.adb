with Text_IO; use Text_IO;

procedure Float_Enter (R : out Result; 
                       Prompt : String := "";
                       Help : Boolean := False) is
  package Result_IO is new Float_IO (Result);
  use Result_IO;
begin
  loop
    begin
      Put (Prompt);
      if Help then 
        Put (" [");
        Put (Result'First);
        Put ("..");
        Put (Result'Last);
        Put ("] ");
      end if;
      Put ("? ");
      Get (R);
      Skip_Line;
      exit;
    exception
      when Data_Error | Constraint_Error =>
        Skip_Line;
    end;
  end loop;
end Float_Enter;
