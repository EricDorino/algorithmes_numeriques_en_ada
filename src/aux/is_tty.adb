
with Interfaces.C_Streams;
use Interfaces.C_Streams;

function Is_TTY return Boolean is
begin
  return isatty(fileno(stdout)) /= 0;
end Is_TTY;

