
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

generic
  type Result is range <>;
package Menu is
  type Choice is array (Result) of Unbounded_String;
  procedure Enter (C : Choice; N: out Result);
end Menu;
