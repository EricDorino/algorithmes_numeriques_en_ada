
with AUnit; use AUnit;
with AUnit.Test_Cases; use AUnit.Test_Cases;

package Matrix_Tests is

  type Matrix_Test is new Test_Cases.Test_Case with null record;

  procedure Register_Tests (T : in out Matrix_Test);
  function Name (T : Matrix_Test) return Message_String;

end Matrix_Tests;
