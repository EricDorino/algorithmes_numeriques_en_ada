
with Test_Suite;
with AUnit.Run;
with AUnit.Reporter.Text;

procedure Test is

  procedure Run is new AUnit.Run.Test_Runner (Test_Suite.Suite);
  Report : AUnit.Reporter.Text.Text_Reporter;

begin
  Run (Report);
end Test;
