
with Matrix_Tests;

package body Test_Suite is

  Result : aliased AUnit.Test_Suites.Test_Suite;

  Test1 : aliased Matrix_Tests.Matrix_Test;

  function Suite return AUnit.Test_Suites.Access_Test_Suite is
  begin
    AUnit.Test_Suites.Add_Test (Result'access, Test1'access);
    return Result'access;
  end Suite;
  
end Test_Suite;
