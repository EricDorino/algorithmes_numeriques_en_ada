
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Numerics.Generic_Real_Arrays;
with Matrix, Matrix_IO;
with AUnit.Assertions; use AUnit.Assertions;

package body Matrix_Tests is

  package Float_Arrays is new Ada.Numerics.Generic_Real_arrays (Float);
  package Float_Matrix is new Matrix (Float, Float_Arrays);
  package Float_Matrix_IO is new Matrix_IO (Float, Float_Arrays);
  use Float_Arrays, Float_Matrix, Float_Matrix_IO;

  function Is_Hilbert_Matrix (M : Real_Matrix) return Boolean is
    -- M(I, J) = 1 / (I + J - 1)
  begin
    if M'Length (1) /= M'Length (2) or 
       M'First (1) /= 1 or M'First (2) /= 1 then
       return False;
    end if;
    for I in M'Range (1) loop
      for J in M'range (2) loop
        if M (I, J) /= 1.0 / Float (I + J - 1) then
          return False;
        end if;
      end loop;
    end loop;
    return True;
  end Is_Hilbert_Matrix;

  procedure Test_Hilbert_1 (T : in out Test_Cases.Test_Case'class) is
    H : Real_Matrix (1 .. 2, 1 .. 2);
  begin
    Matrice_De_Hilbert (H);
    Assert(Is_Hilbert_Matrix (H), "Test_Hilbert_1 fails");
  end Test_Hilbert_1;

  procedure Test_Hilbert_2 (T : in out Test_Cases.Test_Case'class) is
    H : Real_Matrix (1 .. 5, 1 .. 5);
  begin
    Matrice_De_Hilbert (H);
    Assert(Is_Hilbert_Matrix (H), "Test_Hilbert_2 fails");
  end Test_Hilbert_2;

  function Is_Symmetric (M : Real_Matrix) return Boolean is
  begin
    for I in M'Range (1) loop
      for J in M'range (2) loop
        if M (I, J) /= M (J, I) then
          return False;
        end if;
      end loop;
    end loop;
    return True;
  end Is_Symmetric;

  procedure Test_Symmetric_1 (T : in out Test_Cases.Test_Case'class) is
    M : Real_Matrix (1 .. 3, 1 .. 3);
  begin
    Matrice_Symetrique_Aleatoire (M);
    Assert(Is_Symmetric (M), "Test_Symmetric_1 fails");
  end Test_Symmetric_1;

  procedure Test_Norme_1 (T : in out Test_Cases.Test_Case'class) is
    V : Real_Vector (1 .. 5) := (1.0, 2.0, 3.0, 4.0, 5.0);
  begin
    Assert(7.416198487 = Norme_Euclidienne (V), "Test_Norme_1 fails");
  end Test_Norme_1;

  procedure Test_Diagonale_1 (T : in out Test_Cases.Test_Case'class) is
    M : Real_Matrix (1 .. 3, 1 .. 3);
  begin
    Matrice_Test (M);
    Assert(not Est_Diagonale_Dominante (M), "Test_Diagonale_1 fails");
  end Test_Diagonale_1;

  procedure Test_Diagonale_2 (T : in out Test_Cases.Test_Case'class) is
    M : Real_Matrix (1 .. 3, 1 .. 3);
  begin
    Matrice_De_Hilbert (M);
    Assert(not Est_Diagonale_Dominante (M), "Test_Diagonale_2 fails");
  end Test_Diagonale_2;

  procedure Test_Diagonale_3 (T : in out Test_Cases.Test_Case'class) is
    M : Real_Matrix (1 .. 3, 1 .. 3) :=
      ( (3.0, -2.0, 1.0),
        (1.0, -3.0, 2.0),
        (-1.0, 2.0, 4.0) );
  begin
    Assert(Est_Diagonale_Dominante (M), "Test_Diagonale_3 fails");
  end Test_Diagonale_3;

  procedure Test_Diagonale_4 (T : in out Test_Cases.Test_Case'class) is
    M : Real_Matrix (1 .. 3, 1 .. 3) :=
      ( (-2.0, 2.0, 1.0),
        (1.0, 3.0, 2.0),
        (1.0, -2.0, 0.0) );
  begin
    Assert(not Est_Diagonale_Dominante (M), "Test_Diagonale_4 fails");
  end Test_Diagonale_4;

  procedure Register_Tests (T : in out Matrix_Test) is
    use AUnit.test_Cases.Registration;
  begin
    Register_Routine (T, Test_Hilbert_1'access, "Matrice de Hilbert d'ordre 2");
    Register_Routine (T, Test_Hilbert_2'access, "Matrice de Hilbert d'ordre 5");
    Register_Routine (T, Test_Symmetric_1'access, "Matrice symétrique 3x3");
    Register_Routine (T, Test_Norme_1'access, "Norme euclidienne");
    Register_Routine (T, Test_Diagonale_1'access, "Diagonale dominante (Test)");
    Register_Routine (T, Test_Diagonale_2'access, "Diagonale dominante (Hilbert)");
    Register_Routine (T, Test_Diagonale_3'access, "Diagonale dominante (Vrai)");
    Register_Routine (T, Test_Diagonale_4'access, "Diagonale dominante (Faux)");
  end Register_Tests;

  function Name (T : Matrix_Test) return Message_String is
  begin
    return Format ("Matrix");
  end Name;

end Matrix_Tests;
